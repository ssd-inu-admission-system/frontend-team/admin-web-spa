import React from 'react';
import {connect} from "react-redux";

import {createManagerProfile, deleteManagerProfile, fetchManagersFromServer} from "../../store/staff/actions";
import {getManagers} from "../../store/staff/reducer";
import {getUserToken} from "../../store/user/reducer";
import {ApplicationWrapper} from "../../components/ApplicationWrapper/applicationWrapper";
import {EmployeeList} from "../../components/EmployeeList/employee";

import Modal from '../../components/Modal/Modal';
import ManagerCreationForm from '../../components/StaffCreationForm/StaffCreationForm';
import {pushError} from "../../store/actionCenter/actions";

class ManageStaffPage extends React.Component {
    state = {
        modalIsOpen: false,
        newManagerData: {}
    };

    constructor(props) {
        super(props);
        props.dispatch(fetchManagersFromServer(props.token));
    }

    onWorkerDelete = (staff_id) => () => {
        this.props.dispatch(deleteManagerProfile(staff_id));
    };

    handleChange = (key) => (event) => {
        const newState = {
            ...this.state,
            newManagerData: {
                ...this.state.newManagerData
            }
        };
        newState.newManagerData[key] = event.target.value;

        this.setState(newState);
    };

    saveManager = () => {
        if (this.state.newManagerData.fullName
            && this.state.newManagerData.email
            && this.state.newManagerData.password) {
            this.props.dispatch(createManagerProfile(this.state.newManagerData.fullName,
                this.state.newManagerData.email,
                this.state.newManagerData.password));
            this.setState({modalIsOpen: false});
        } else {
            this.props.dispatch(pushError("Fill in the form to continue"));
        }
    };

    render() {
        return (
            <React.Fragment>
                <ApplicationWrapper
                    headLine="Managers"
                    addButtonHandler={() => this.setState({modalIsOpen: true})}
                >
                    <EmployeeList workers={this.props.managers || false} onWorkerDelete={this.onWorkerDelete}/>
                </ApplicationWrapper>
                {this.state.modalIsOpen ? (
                    <Modal
                        onCancelClick={() => this.setState({modalIsOpen: false})}
                        onSaveClick={this.saveManager}
                        title={"Create Manager"}
                    >
                        <ManagerCreationForm
                            data={this.state.newManagerData}
                            handleChange={this.handleChange}
                        />
                    </Modal>
                ) : ''}
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        managers: getManagers(state),
        token: getUserToken(state)
    }
};


export default connect(mapStateToProps)(ManageStaffPage);
