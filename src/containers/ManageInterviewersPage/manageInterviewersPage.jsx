import React from 'react';
import {connect} from "react-redux";

import {
    createInterviewerProfile,
    deleteInterviewerProfile,
    fetchInterviewersFromServer
} from "../../store/staff/actions";
import {getInterviewers} from "../../store/staff/reducer";
import {getUserToken} from "../../store/user/reducer";
import {ApplicationWrapper} from "../../components/ApplicationWrapper/applicationWrapper";
import {EmployeeList} from "../../components/EmployeeList/employee";
import {pushError} from "../../store/actionCenter/actions";
import Modal from "../../components/Modal/Modal";
import ManagerCreationForm from "../../components/StaffCreationForm/StaffCreationForm";

class ManageInterviewersPage extends React.Component {


    constructor(props) {
        super(props);

        props.dispatch(fetchInterviewersFromServer());
    }

    state = {
        modalIsOpen: false,
        newInterviewerData: {}
    };

    onWorkerDelete = (staff_id) => () => {
        this.props.dispatch(deleteInterviewerProfile(staff_id));
    };

    handleChange = (key) => (event) => {
        const newState = {
            ...this.state,
            newInterviewerData: {
                ...this.state.newInterviewerData
            }
        };
        newState.newInterviewerData[key] = event.target.value;

        this.setState(newState);
    };

    saveInterviewer = () => {
        if (this.state.newInterviewerData.fullName
            && this.state.newInterviewerData.email
            && this.state.newInterviewerData.password) {
            this.props.dispatch(createInterviewerProfile(this.state.newInterviewerData.fullName,
                this.state.newInterviewerData.email,
                this.state.newInterviewerData.password));
            this.setState({
                modalIsOpen: false,
                newInterviewerData: {}
            });
        } else {
            this.props.dispatch(pushError("Fill in the form to continue"));
        }
    };

    render() {
        return (
            <React.Fragment>
                <ApplicationWrapper
                    headLine="Interviewers"
                    addButtonHandler={() => this.setState({modalIsOpen: true})}
                >
                    <EmployeeList workers={this.props.interviewers || false} onWorkerDelete={this.onWorkerDelete}/>
                </ApplicationWrapper>
                {this.state.modalIsOpen ? (
                    <Modal
                        onCancelClick={() => this.setState({modalIsOpen: false})}
                        onSaveClick={this.saveInterviewer}
                        title={"Create Interviewer"}
                    >
                        <ManagerCreationForm
                            data={this.state.newInterviewerData}
                            handleChange={this.handleChange}
                        />
                    </Modal>
                ) : ''}
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        interviewers: getInterviewers(state),
        token: getUserToken(state)
    }
};


export default connect(mapStateToProps)(ManageInterviewersPage);
