import React from 'react';

import styles from './applicationsListsPage.module.css';
import {connect} from "react-redux";
import {ApplicationWrapper} from "../../components/ApplicationWrapper/applicationWrapper";
import {getApplications} from "../../store/applications/reducer";
import person from "../../assets/round1.png";
import {LoadingWidget} from "../../components/LoadingWIdget/loadingWidget";
import {fetchApplications, removeCurrentApplication} from "../../store/applications/actions";

export function getStatus(code) {
    switch (code) {
        case 0:
            return 'applied';
        case 1:
            return 'on interview';
        case 2:
            return 'approved';
        case 3:
            return 'rejected';
        default:
            return 'all';
    }
}


class ApplicationsListsPage extends React.Component {

    constructor(props) {
        super(props);

        props.dispatch(fetchApplications());
        props.dispatch(removeCurrentApplication());
    }

    // -1 - all
    // 0 - applied
    // 1 - interviewed
    // 2 - approved
    // 3 - rejected
    state = {
        selectedApplicationsStatus: -1,
    };

    onSelectedStatusChange = (statusCode) => () => {
        let newState = {
            ...this.state,
            selectedApplicationsStatus: statusCode,
        };

        this.setState(newState);
    };

    getNeededApplications() {
        if (this.state.selectedApplicationsStatus === -1)
            return this.props.applications;
        let result = [];
        const status = this.state.selectedApplicationsStatus;
        this.props.applications.forEach((appl) => {
            if (appl.application_status === status)
                result.push(appl);
        });
        return result;
    }

    onApplicationClick = (id) => () => {
        this.props.history.push(`/application/${id}`);
    };

    render() {
        if (!this.props.applications) {
            return (
                <ApplicationWrapper headLine="Applications">
                    <LoadingWidget/>
                </ApplicationWrapper>
            );
        }
        return (
            <ApplicationWrapper headLine="Applications">
                <div className={styles.statuses}>
                    <div>Status:</div>
                    {[-1, 0, 1, 2, 3].map((code) => {
                        return (
                            <button className={styles.textButton} key={code}
                                    onClick={this.onSelectedStatusChange(code)}>
                                {getStatus(code)}
                            </button>);
                    })}
                </div>
                {this.getNeededApplications().map((application) => {
                    return (
                        <ApplicationComponent
                            key={application.application_id}
                            application={application}
                            onApplicationClick={this.onApplicationClick}/>
                    );
                })}
            </ApplicationWrapper>
        );
    }
}

const ApplicationComponent = (props) => {
    const application = props.application;
    return (
        <div className={styles.container}
             onClick={props.onApplicationClick(application.application_id)}>
            <img className={styles.userPhoto} src={person} alt="person"/>
            <div className={styles.personInfo}>
                <span className={styles.nameSurname}>
                    <div>
                        {`${application.applicant_name} ${application.applicant_surname}`}
                    </div>
                    <div>
                        {`ID ${application.application_id}`}
                    </div>
                </span>
            </div>
            <div className={styles.status}>
                <span>Status: {getStatus(application.application_status)}</span>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        applications: getApplications(state),
    }
};

export default connect(mapStateToProps)(ApplicationsListsPage);

