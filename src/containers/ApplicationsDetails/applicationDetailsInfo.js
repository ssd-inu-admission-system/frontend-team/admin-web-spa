import React from 'react';
import styles from "./applicationsDetails.module.css";
import document from "../../assets/text-document.png";
import {BASE_URL} from "../../services/apiConnector";

export const PersonalData = (props) => {
    const list = [
        {key: "Email", valueKey: "email"},
        {key: "Phone", valueKey: "phone"},
        {key: "Citizenship", valueKey: "citizenship"},
        {key: "Country of living", valueKey: "country"},
        {key: "City", valueKey: "city"},
        {key: "Source", valueKey: "source"},
    ];

    return (
        <div className={styles.fieldsContainer}>
            <div className={styles.fieldsNames}>
                <div>
                    Full Name:
                </div>
                <div>
                    Date of birth:
                </div>
                <div>
                    Gender:
                </div>
                {list.map((object) => {
                    return (
                        <div key={object.valueKey}>
                            {object.key}:
                        </div>
                    );
                })}
            </div>
            <div>
                <div>
                    {`${props.currentApplication.user.first_name} 
                      ${props.currentApplication.user.middle_name} 
                      ${props.currentApplication.user.last_name}`}
                </div>
                <div>
                    {props.currentApplication.user.birth_date || "-"}
                </div>
                <div>
                    {props.currentApplication.user.gender ? 'Male' : 'Female'}
                </div>
                {list.map((object) => {
                    return (
                        <div key={object.valueKey}>
                            {props.currentApplication.user[object.valueKey] || "-"}
                        </div>
                    );
                })}
            </div>
        </div>
    );
};


export const EducationData = (props) => {
    const list = [
        {key: "Country of Study", valueKey: "school_country"},
        {key: "City of Study", valueKey: "school_city"},
        {key: "Name of School", valueKey: "school_name"},
        {key: "Graduation Year (actual or expected)", valueKey: "graduation_year"},
        {key: "Average Grade (optional)", valueKey: "grade_average"},
        {key: "Russian Test Year (optional)", valueKey: "year_russian"},
        {key: "Russian Test Result (optional)", valueKey: "grade_russian"},
        {key: "Math Test Year (optional)", valueKey: "year_math"},
        {key: "Math Test Result (optional)", valueKey: "grade_math"},
        {key: "Informatics/Physics Test Year (optional)", valueKey: "year_info_physics"},
        {key: "Informatics/Physics Test Result (optional)", valueKey: "grade_info_physics"},
        {key: "Date of  Diploma/School Leaving Certificate Issue (optional)", valueKey: "date_diploma"},
    ];
    return (
        <div className={styles.fieldsContainer}>
            <div className={styles.fieldsNames}>
                {list.map((object) => {
                    return (
                        <div key={object.key}>
                            {object.key}:
                        </div>
                    );
                })}
            </div>
            <div>
                {list.map((object) => {
                    return (
                        <div key={object.valueKey}>
                            {props.currentApplication.user[object.valueKey] || "-"}
                        </div>
                    );
                })}
            </div>
        </div>
    );
};


export const Tests = (props) => {
    return (
        <div>
            {props.currentApplication.application.tests_info.map((object) => {
                return (
                    <div key={object.test_id}>
                        {object.title}: {object.correct} out of {object.total} correct answers
                    </div>
                );
            })}
        </div>
    );
};

export const Documents = (props) => {
    const document_types = {
        1: 'CV/portfolio',
        2: 'Motivation Letter',
        3: 'Passport Scan',
        4: 'Transcripts',
        5: 'Recommendations',
        6: 'Project Description',
    };
    console.log(props.currentApplication.application.documents);
    return (
        <div className={styles.documentsContainer}>
            {props.currentApplication.application.documents.map((object) => {
                return (
                    <div className={styles.documentContainer}
                         onClick={(event) => {
                             // event.preventDefault();
                             event.stopPropagation();
                             window.open(`${BASE_URL}\\${object.document_url}?token=${props.token}`, '_blank');
                         }}
                         key={object.document_id}>
                        <div style={{margin: 8}}>
                            {document_types[object.document_type]}
                        </div>
                        <img src={document} alt="document"/>
                        <div style={{margin: 8}}>
                            {object.document_name}
                        </div>
                    </div>
                );
            })}
        </div>
    );
};
