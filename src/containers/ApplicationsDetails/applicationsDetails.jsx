import React from 'react';


import {connect} from "react-redux";
import {changeApplicationStatus, fetchApplication} from "../../store/applications/actions";
import {ApplicationWrapper} from "../../components/ApplicationWrapper/applicationWrapper";
import {LoadingWidget} from "../../components/LoadingWIdget/loadingWidget";
import {getCurrentApplication} from "../../store/applications/reducer";
import {ApplicationBar} from "./ApplicationBar/applicationBar";
import {PersonalData, EducationData, Documents, Tests} from './applicationDetailsInfo';
import InterviewerComment from '../InterviewerComment/interviewerComment'

import styles from './applicationsDetails.module.css';
import { Accordion, AccordionMenu, Panel } from '../../components/Accordion/accordion';
import {getUserToken, getUserType} from "../../store/user/reducer";

class ApplicationsDetails extends React.Component {

    information = [
        {
            component: PersonalData,
            title: "Personal Data"
        },
        {
            component: EducationData,
            title: "Education"
        },
        {
            component: Tests,
            title: "Tests"
        },
        {
            component: Documents,
            title: "Documents"
        },
    ];

    state = {
        isDialogActive: false,
    };

    constructor(props) {
        super(props);
        props.dispatch(fetchApplication((this.props.match.params.id)));
    }

    changeStatus = (event) => {
        this.props.dispatch(changeApplicationStatus(this.props.match.params.id, event.target.value));
    };

    render() {
        if (!this.props.currentApplication.user && !this.props.currentApplication.application) {
            return (
                <ApplicationWrapper headLine={`Application ID ${this.props.match.params.id}`}>
                    <LoadingWidget/>
                </ApplicationWrapper>
            );
        }
        return (
            <ApplicationWrapper headLine={`Application ID ${this.props.match.params.id}`}>
                <ApplicationBar
                    user={this.props.currentApplication.user}
                    application={this.props.currentApplication.application}
                    handleStatusChange={this.changeStatus}
                    applicationId={this.props.match.params.id}
                    accountType={this.props.accountType}
                />
                {this.information.map((data) => {
                    return (
                        <div key={data.title}>
                            <AccordionMenu>
                                <Accordion title={data.title} className={styles.titleBox}>
                                    <Panel>
                                        <hr/>
                                        <div className={styles.accordionBody} onClick={(event => event.stopPropagation())}>
                                            {data.component(this.props)}
                                        </div>
                                    </Panel>
                                </Accordion>
                            </AccordionMenu>
                        </div>

                    )
                })}
                <InterviewerComment
                    comment={this.props.currentApplication.application.comment}
                    grade={this.props.currentApplication.application.grade}
                    applicationInterviewer={this.props.currentApplication.application.interviewer_id}
                    applicationID={this.props.match.params.id}/>
            </ApplicationWrapper>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        currentApplication: getCurrentApplication(state),
        accountType: getUserType(state),
        token: getUserToken(state)
    }
};

export default connect(mapStateToProps)(ApplicationsDetails);
