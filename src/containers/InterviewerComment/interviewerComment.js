import React from 'react';
import {connect} from "react-redux";

import styles from './interviewerComment.module.css';
import {getUserID, getUserType} from "../../store/user/reducer";
import {submitCommentToApplication, submitGradeToApplication} from "../../store/applications/actions";


class InterviewerComment extends React.Component {

    state = {
        comment: undefined,
        grade: undefined,
    };

    constructor(props) {
        super(props);
        this.state.comment = props.comment;
        this.state.grade = props.grade;
    }

    render() {
        const enabled = this.props.accountType === 2 && this.props.applicationInterviewer === this.props.accountID;
        return (
            <div>
                <div className={styles.text}>Grade to the application (from 0 to 100):</div>
                <input
                    className={styles.inputTextFiled}
                    value={this.state.grade || ""}
                    disabled={!enabled}
                    onChange={this.onGradeChange}/>
                <div className={styles.text}>Comment from interviewer:</div>
                <textarea
                    rows={4}
                    className={styles.inputTextFiled}
                    value={this.state.comment || ""}
                    disabled={!enabled}
                    onChange={this.onCommentChange}/>
                <div className={styles.buttons}>
                    <button onClick={this.onGradeSubmit} className={styles.buttonName}>Submit grade</button>
                    <button onClick={this.onCommentSubmit} className={styles.buttonName}>Submit comment</button>
                </div>
            </div>
        );
    }

    onGradeChange = (event) => {
        const a = Number.isInteger(Number(event.target.value));
        const b = Number(event.target.value) >= 0;
        const c = Number(event.target.value) <= 100;
        if (a && b && c) {
            this.setState({
                ...this.state,
                grade: Number(event.target.value)
            })
        }
    };

    onCommentChange = (event) => {
        this.setState({
            ...this.state,
            comment: event.target.value
        })
    };

    onGradeSubmit = () => {
        this.props.dispatch(submitGradeToApplication(this.props.applicationID, Number(this.state.grade)));
    };

    onCommentSubmit = () => {
        this.props.dispatch(submitCommentToApplication(this.props.applicationID, this.state.comment));
    };
}

const mapStateToProps = (state) => {
    return {
        accountType: getUserType(state),
        accountID: getUserID(state),
    }
};

export default connect(mapStateToProps)(InterviewerComment);
