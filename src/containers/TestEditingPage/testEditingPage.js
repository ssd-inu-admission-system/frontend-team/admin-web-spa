import React from 'react';
import {ApplicationWrapper} from '../../components/ApplicationWrapper/applicationWrapper';
import {TestTitleForm} from '../../components/TestTitleForm/testTitleForm';
import {TestQuestionForm} from "../../components/TestQuestionForm/testQuestionForm";
import {connect} from "react-redux";
import {fetchTestInfoFromServer, updateTest} from "../../store/tests/actions";

import styles from './testEditingPage.module.css';
import {pushError} from "../../store/actionCenter/actions";
import {LoadingWidget} from "../../components/LoadingWIdget/loadingWidget";
import {getTestsInfo} from "../../store/tests/reducer";

class TestEditingPage extends React.Component {

    constructor(props) {
        super(props);

        this.props.dispatch(fetchTestInfoFromServer(this.props.match.params.id));
    }

    state = {
        test: undefined
    };

    render() {
        if (!this.state.test) {
            return (<LoadingWidget/>);
        }
        return (
            <ApplicationWrapper headLine="Create Test">
                <TestTitleForm
                    title={this.state.test.title}
                    description={this.state.test.description}
                    onTitleChange={this.onTitleChange}
                    onDescriptionChange={this.onDescriptionChange}/>
                {this.state.test.questions.map((question, index) => {
                    return <TestQuestionForm
                        key={question.number}
                        question={question}
                        onQuestionDelete={this.onQuestionDelete(index)}
                        onQuestionChange={this.onQuestionChangeCreator(index)}/>
                })}
                <div>
                    <button onClick={this.addNewQuestion} className={styles.buttonName}>Add new question</button>
                    <button onClick={this.sendNewTestToServer} className={styles.buttonName}>EDIT TEST</button>
                </div>
            </ApplicationWrapper>
        )
    }

    static getDerivedStateFromProps(props, state) {
        if (typeof state.test === "undefined" && typeof props.test !== "undefined") {
            // put test from props to state
            return {
                ...state,
                test: {
                    ...props.test,
                }
            };
        } else if (props.test && state.test && props.test.test_id !== state.test.test_id) {
            // update state when new test is loaded (for example going from /edit_test/1 to /edit_test/2)
            return {
                ...state,
                test: {
                    ...props.test
                },
            };
        }
        return null;
    }

    onTitleChange = (event) => {
        this.setState({
            ...this.state,
            test: {
                ...this.state.test,
                title: event.target.value
            }
        });
    };

    onDescriptionChange = (event) => {
        this.setState({
            ...this.state,
            test: {
                ...this.state.test,
                description: event.target.value
            }
        });
    };

    addNewQuestion = () => {
        const number = this.getUnusedQuestionNumber();
        let newState = {
            ...this.state
        };
        newState.test.questions.push({
            number: number,
            text: "",
            type: 1, // default type is single choice
            options: [
                {
                    text: "", is_correct: false
                },]
        });
        this.setState(newState);
    };

    onQuestionChangeCreator = (index) => (key, newValue) => {
        let newState = {
            ...this.state
        };
        newState.test.questions[index][key] = newValue;
        this.setState(newState);
    };

    onQuestionDelete = (index) => () => {
        let newState = {
            ...this.state,
            test: {
                ...this.state.test,
                questions: this.state.test.questions
            }
        };
        console.log(newState.test);
        newState.test.questions.splice(index, 1);
        this.setState(newState);
    };

    displayError = (error) => {
        this.props.dispatch(pushError(error));
    };

    sendNewTestToServer = () => {
        let questionsCorrect = true;
        if (this.state.test.title.length === 0) {
            this.displayError("title is not correct");
            questionsCorrect = false;
        } else if (this.state.test.description.length === 0) {
            this.displayError("description is not correct");
            questionsCorrect = false;
        }
        this.state.test.questions.forEach((question, index) => {

            let noAnswerChecked = true;
            if (question.text.length === 0) {
                this.displayError(`${index} question is not correct`);
                questionsCorrect = false;
            }
            question.options.forEach((option, optionIndex) => {
                if (option.text.length === 0) {
                    this.displayError(`${index} question, ${optionIndex} option is not correct`);
                    questionsCorrect = false;
                }
                if (option.is_correct) {
                    noAnswerChecked = false;
                }
            });

            if (noAnswerChecked) {
                questionsCorrect = false;
                this.displayError(`${index} question no answer selected`);
            }
        });
        if (questionsCorrect) {
            const newTest = this.preprocessTest();
            console.log(newTest);
            this.props.dispatch(updateTest(this.props.match.params.id, newTest));
        }
    };

    preprocessTest() {
        let newTest = {
            title: this.state.test.title,
            description: this.state.test.description
        };

        newTest.questions = this.state.test.questions.map((question, index) => {
            const options = question.options.map((option, index) => {
                return {
                    ...option,
                    number: index
                };
            });
            return {
                ...question,
                number: index,
                options: options
            };
        });
        return newTest;
    }

    getUnusedQuestionNumber() {
        let number = 1;
        this.state.test.questions.forEach((question) => {
            if (Number(question.number) > Number(number)) {
                number = question.number;
            }
        });
        return number + 1;
    }
}

const mapStateToProps = (state) => {
    return {
        test: getTestsInfo(state),
    }
};

export default connect(mapStateToProps)(TestEditingPage);