import React from 'react';

import {LoginStaticPage} from '../../components/LoginStaticPage/loginStaticPage';
import {SignInForm} from '../../components/SignInForm/signInForm';
import * as actions from '../../store/user/actions';

import styles from './loginPage.module.css';
import {connect} from "react-redux";

class LoginPage extends React.Component {

    state = {
        signInForm: {
            email: "admin@admin.ru",
            password: "admin"
        },
    };

    onSignInSubmit = (event) => {
        event.preventDefault();
        this.props.dispatch(
            actions.fetchTokenFromServer(this.state.signInForm.email,
                this.state.signInForm.password)
        );
    };

    handleSignInChange = (key) => (event) => {
        const newState = {
            ...this.state,
            signInForm: {
                ...this.state.signInForm
            }
        };
        newState.signInForm[key] = event.target.value;

        this.setState(newState);
    };

    render() {
        return (
            <div className={styles.mainContainer}>
                <div className={styles.mainContainerBlur}>
                    <div className={styles.firstChild}>
                        <LoginStaticPage/>
                    </div>
                        <SignInForm data={this.state.signInForm}
                                    handleChange={this.handleSignInChange}
                                    onSubmit={this.onSignInSubmit}/>
                </div>
            </div>
        )
    }
}

export default connect()(LoginPage);