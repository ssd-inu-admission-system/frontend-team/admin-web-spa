import React from 'react';

import {ApplicationWrapper} from '../../components/ApplicationWrapper/applicationWrapper';

import { CreateEmployeeForm } from "../../components/CreateEmployeeForm/createEmployeeForm";

export class CreateEmployeePage extends React.Component {

    state = {
        employeeData: {}
    };

    handleChange = (key) => (event) => {
        const newState = {
            ...this.state,
            employeeData: {
                ...this.state.employeeData
            }
        };
        newState.employeeData[key] = event.target.value;

        this.setState(newState);
    };

    onSubmit = (event) => {
        event.preventDefault();
    };

    render() {
        return (
            <ApplicationWrapper>
                <CreateEmployeeForm
                    data={this.state.employeeData}
                    handleChange={this.handleChange}
                    onSubmit={this.onSubmit}/>
            </ApplicationWrapper>
        )
    }
}