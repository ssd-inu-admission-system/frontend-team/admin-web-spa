import React from 'react';
import {ApplicationWrapper} from '../../components/ApplicationWrapper/applicationWrapper';
import {TestTitleForm} from '../../components/TestTitleForm/testTitleForm';
import {TestQuestionForm} from "../../components/TestQuestionForm/testQuestionForm";
import {connect} from "react-redux";
import {createNewTest} from "../../store/tests/actions";

import styles from './testCreationPage.module.css';
import {pushError} from "../../store/actionCenter/actions";

class TestCreationPage extends React.Component {

    /*
    0 - open
    1 - single-choice
    2 - multiple-choice
    */
    state = {
        title: "",
        description: "",
        questions: [
            {
                text: "",
                type: 1, // default type is single choice
                options: [
                    {
                        text: "", is_correct: false
                    }
                ]
            }
        ],
    };

    render() {
        return (
            <ApplicationWrapper headLine="Create Test">
                <TestTitleForm onTitleChange={this.onTitleChange} onDescriptionChange={this.onDescriptionChange}/>
                {this.state.questions.map((question, i) => {
                    return <TestQuestionForm
                        key={i}
                        question={question}
                        onQuestionDelete={this.onQuestionDelete(i)}
                        onQuestionChange={this.onQuestionChangeCreator(i)}/>
                })}
                <div>
                    <button onClick={this.addNewQuestion} className={styles.buttonName}>Add new question</button>
                    <button onClick={this.sendNewTestToServer} className={styles.buttonName}>CREATE TEST</button>
                </div>
            </ApplicationWrapper>
        )
    }

    onTitleChange = (event) => {
        let newState = {
            ...this.state
        };
        newState.title = event.target.value;
        this.setState(newState);
    };

    onDescriptionChange = (event) => {
        let newState = {
            ...this.state
        };
        newState.description = event.target.value;
        this.setState(newState);
    };

    addNewQuestion = () => {
        let newState = {
            ...this.state
        };
        newState.questions.push({
            text: "",
            type: 1, // default type is single choice
            options: [
                {
                    text: "", is_correct: false
                },]
        });
        this.setState(newState);
    };

    onQuestionChangeCreator = (id) => (key, newValue) => {
        let newState = {
            ...this.state
        };
        newState.questions[id][key] = newValue;
        this.setState(newState);
    };

    onQuestionDelete = (index) => () => {
        let newState = {
            ...this.state,
            questions: this.state.questions
        };
        newState.questions.splice(index, 1);
        this.setState(newState);
    };


    displayError = (error) => {
        this.props.dispatch(pushError(error));
    };

    sendNewTestToServer = () => {
        let questionsCorrect = true;
        if (this.state.title.length === 0) {
            this.displayError("title is not correct");
            questionsCorrect = false;
        } else if (this.state.description.length === 0) {
            this.displayError("description is not correct");
            questionsCorrect = false;
        }
        this.state.questions.forEach((question, index) => {

            let noAnswerChecked = true;
            if (question.text.length === 0) {
                this.displayError(`${index} question is not correct`);
                questionsCorrect = false;
            }
            question.options.forEach((option, optionIndex) => {
                if (option.text.length === 0) {
                    this.displayError(`${index} question, ${optionIndex} option is not correct`);
                    questionsCorrect = false;
                }
                if (option.is_correct) {
                    noAnswerChecked = false;
                }
            });

            if (noAnswerChecked) {
                questionsCorrect = false;
                this.displayError(`${index} question no answer selected`);
            }
        });
        if (questionsCorrect) {
            const newTest = this.preprocessTest();
            console.log(newTest);
            this.props.dispatch(createNewTest(newTest));
        }
    };

    preprocessTest() {
        let newTest = {
            title: this.state.title,
            description: this.state.description
        };

        newTest.questions = this.state.questions.map((question, index) => {
            const options = question.options.map((option, index) => {
                return {
                    ...option,
                    number: index
                };
            });
            return {
                ...question,
                number: index,
                options: options
            };
        });
        return newTest;
    }
}

export default connect()(TestCreationPage);