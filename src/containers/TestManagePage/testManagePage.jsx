import React from 'react';
import { ApplicationWrapper } from '../../components/ApplicationWrapper/applicationWrapper';
import { TestManaging } from '../../components/TestManaging/testManaging';
import {connect} from "react-redux";
import {LoadingWidget} from "../../components/LoadingWIdget/loadingWidget";
import {deleteTest, fetchTestsFromServer} from "../../store/tests/actions";
import {getTests} from "../../store/tests/reducer";

class TestManagePage extends React.Component {

    constructor(props) {
        super(props);
        props.dispatch(fetchTestsFromServer());
    }

    render() {
        if (!this.props.testsList) {
            return (<LoadingWidget/>);
        }
        return (
            <ApplicationWrapper headLine="Manage Tests">
                <TestManaging testsList={this.props.testsList} onTestDelete={this.onTestDelete} />
            </ApplicationWrapper>
        )
    }

    onTestDelete = (id) => () => {
        this.props.dispatch(deleteTest(id));
    }
}

const mapStateToProps = (state) => {
    return {
        testsList: getTests(state),
    }
};

export default connect(mapStateToProps)(TestManagePage);