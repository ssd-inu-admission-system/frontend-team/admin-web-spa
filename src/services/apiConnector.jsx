// This file is needed to describe all the calls to API. Example is below

import axios from 'axios';


export const BASE_URL = 'http://10.90.138.174:5000';

function checkString(string) {
    return typeof (string) == 'string' &&
    string.trim().length > 0 ?
        string.trim() : false;
}
/*
TODO: may be decommented if needed
function checkNumber(number) {
    return typeof (number) == 'number' &&
    number > 0 ?
        number : false;
}
*/

// Implementation of request to /staff/login
// Get authorization token from server
// Required data: email, password
export async function signIn(email, password_hash) {
    if (email && password_hash) {
        return await post(
            {
                email,
                password_hash
            },
            '/staff/login');
    } else {
        return {
            status: -1,
            error: "Missed required data"
        }
    }
}

////////////////////////////////////////////////////////////////////
/// Managers                                                     ///
////////////////////////////////////////////////////////////////////

// Implementation of request to /managers
// Get a list of all manager IDs and full_names
// Required data: string superadmin_token
export async function getManagersList(token) {
    if (token) {
        return await get({token}, `/managers`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to managers/{id}/info
// Get information about a manager
// Required data: superadmin_token, id
export async function getFullManagerInfo(token, id) {
    if (token) {
        return await get({token}, `/managers/${id}/info`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /managers/new
// Create a new manager in the system
// Required
// token: superadmin_token
// data: object that includes following fields:
// - fullName
// - email
// - passwordHash
export async function newManager(token, data) {
    const full_name = checkString(data.fullName);
    const email = checkString(data.email);
    const password_hash = checkString(data.passwordHash);
    if (email && password_hash && full_name) {

        return await postWithParams(
            {
                email,
                password_hash,
                full_name
            },
            {token},
            '/managers/new');
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /managers/{id}/delete
// Removes a manager from the system
// Required data: superadmin_token, id
export async function deleteManager(token, id) {
    if (token && id) {
        return await get({token}, `/managers/${id}/delete`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

////////////////////////////////////////////////////////////////////
/// Interviewer                                                  ///
////////////////////////////////////////////////////////////////////

// Implementation of request to /interviewers
// Get a list of all interviewer IDs
// Required data: superadmin_token
export async function getInterviewersList(token) {
    if (token) {
        return await get({token}, `/interviewers`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /interviewers/{id}/info
// Get information about an interviewer by id
// Required data: superadmin_token, id
export async function getInterviewerInfo(token, id) {
    if (token) {
        return await get({token}, `/interviewers/${id}/info`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /interviewers/new
// Create a new interviewer in the system
// Required
// token: superadmin_token
// data: object that includes following fields:
// - fullName
// - email
// - passwordHash
export async function newInterviewer(token, data) {
    const full_name = checkString(data.fullName);
    const email = checkString(data.email);
    const password_hash = checkString(data.passwordHash);
    if (email && password_hash && full_name) {

        return await postWithParams(
            {
                email,
                password_hash,
                full_name
            },
            {token},
            '/interviewers/new');
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /interviewers/{id}/delete
// Remove an interviewer from the system
// Required data: superadmin_token, id
export async function deleteInterviewer(token, id) {
    if (token && id) {
        return await get({token}, `/interviewers/${id}/delete`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

////////////////////////////////////////////////////////////////////
/// Users                                                        ///
////////////////////////////////////////////////////////////////////


// Implementation of request to /users
// Get a list of all users (applicants) in the system
// Required data: token
export async function getUsers(token) {
    if (token) {
        return await get({token}, `/users`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /users/{id}/info
// Get profile information for a user
// Required data: token, id
export async function getUserInfo(token, id) {
    if (token && id) {
        return await get({token}, `/users/${id}/info`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

////////////////////////////////////////////////////////////////////
/// Applications                                                 ///
////////////////////////////////////////////////////////////////////


// Implementation of request to /applications
// Get a list of all applications (currently only one program, program id might be needed later)
// Required data: application id, token
/*
{
 “status”: int,
 “error_message”: string,
 “applicant_id”: int,
 “program_id”: int,
 “tests_info”: [
  “test_id”: int,
  “correct”: int,
  “total”: int,
  “grade”: float
 ],
 “documents”: [
  “document_id”: int
  “document_type”: string,
  “document_name”: string,
  “document_url”: string
 ]
}
 */
export async function getApplications(token) {
    if (token) {
        return await get({token}, `/applications`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /applications/{id}/info
// Get an information for a particular application
// Required data: token
export async function getApplication(id, token) {
    if (token && id) {
        return await get({token}, `/applications/${id}/info`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /interviewers/{interviewer_id}/assign
// Get an information for a particular application
// Required data: application_id, token, userID
export async function assignInterviewer(application_id, token, userID) {
    if (token && (typeof application_id !== "undefined") && (typeof userID !== "undefined")) {
        return await postWithParams({application_id}, {token}, `/interviewers/${userID}/assign`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /applications/{id}/grade
// Add a grade to the application from an interviewer from 0 to 100
// Required data: token
export async function sendApplicationGrade(id, token, grade) {
    if (token && (typeof id !== "undefined") && typeof grade !== "undefined") {
        return await postWithParams({grade}, {token}, `/applications/${id}/grade`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /applications/{id}/comment
// Add a grade to the application from an interviewer from 0 to 100
// Required data: token
export async function sendApplicationComment(id, token, comment) {
    if (token && typeof id !== "undefined" && comment) {
        return postWithParams({comment}, {token}, `/applications/${id}/comment`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /applications/{id}/test_results
// Change status of an application
// Required data: token, status, applicationID
export async function changeApplicationStatus(applicationId, status, token) {
    if (applicationId && status !==undefined && token) {
        return await postWithParams(
            {status},
            {token},
            `/applications/${applicationId}/change_status`
        );
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

//----------------------------
// TESTS
// ---------------------------

// Implementation of request to /tests/new
// Create a new test (currently only one program, program id might be needed later)
// Required data: token, test
export async function createNewTest(test, token) {
    if (test && token) {
        return await postWithParams(test, {token}, `/tests/new`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /tests
// Get a list of all tests
// Required data: token
export async function getTests(token) {
    if (token) {
        return await get({token}, `/tests`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /tests/{id}/delete
// Delete a test
// Required data: token, id
export async function deleteTest(token, id) {
    if ((typeof token !== "undefined") && (typeof id !== "undefined")) {
        return await postWithParams(null, {token}, `/tests/${id}/delete`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /tests/{id}/info
// Get information about a test (not the results)
// Required data: token, id
export async function getTestInfo(token, id) {
    if (token && id) {
        return await get({token, id}, `/tests/${id}/info`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// Implementation of request to /tests/{id}/update
// Update information of a test
// Required data: token, id, test
export async function updateTest(token, id, test) {
    if (token && id && test) {
        return await postWithParams({test}, {token}, `/tests/${id}/update`);
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

////////////////////////////////////////////////////////////////////
/// Help functions                                               ///
////////////////////////////////////////////////////////////////////


// url will be formed as BASE_URL + URL
// params is an object with params. OBJECT, this is important
async function get(params, URL) {
    if (params && URL) {
        const url = BASE_URL + URL;

        try {
            const response = await axios.get(url, {
                    params: params,
                    headers: {'Content-Type': 'application/json'}
                }
            );

            if (response.status === 200) {
                return response.data;
            } else {
                return {
                    status: -1,
                    error_message: 'Something went wrong... Server returned code '
                        + response.status
                };
            }
        } catch (e) {
            return {
                status: -1,
                error_message: 'Something went wrong... ' + e
            };
        }
    } else {
        return {
            status: -1,
            error_message: "Missed required data"
        }
    }
}

// data is Object
// url will be formed as BASE_URL + URL
async function post(data, URL) {
    const url = BASE_URL + URL;
    try {
        const response = await axios.post(url, data, {
                headers: {'Content-Type': 'application/json'}
            }
        );

        if (response.status === 200) {
            return response.data;
        } else {
            return {
                status: -1,
                error_message: 'Something went wrong... Server returned code '
                    + response.status
            };
        }
    } catch (e) {
        return {
            status: -1,
            error_message: 'Something went wrong... ' + e
        };
    }
}

// this function is similar to function above
// needed just to be sure
// TODO: may be remove this function
// data is Object
// url will be formed as BASE_URL + URL
// params is an object with params. OBJECT, this is important
async function postWithParams(data, params, URL) {
    const url = BASE_URL + URL;
    try {
        const response = await axios.post(url, data, {
                headers: {'Content-Type': 'application/json'},
                params: params
            }
        );

        if (response.status === 200) {
            return response.data;
        } else {
            return {
                status: -1,
                error_message: 'Something went wrong... Server returned code '
                    + response.status
            };
        }
    } catch (e) {
        return {
            status: -1,
            error_message: 'Something went wrong... ' + e
        };
    }
}