import * as api from '../../services/apiConnector';
import * as types from './actionTypes';

import * as actionCenter from '../actionCenter/actions';

import {getUserToken} from "../user/reducer";


export function fetchManagersFromServer(token) {
  return async (dispatch, getState) => {
    dispatch(actionCenter.startAction("fetching managers"));

    const resp = await api.getManagersList(token);
    if (resp.status === 0) {
      let data = {
        managers: []
      };
      resp.staff.forEach((singleStaff) => {
        data.managers.push({
          staff_id: singleStaff.staff_id,
          fullName: singleStaff.full_name
        })
      });

      dispatch({
        type: types.MANAGERS_FETCHED,
        ...data
      });
      dispatch(actionCenter.finishAction("fetching managers"));
    } else {
      dispatch(actionCenter.failAction("fetching managers", resp.error_message));
    }
  }
}

export function fetchInterviewersFromServer() {
  return async (dispatch, getState) => {
    dispatch(actionCenter.startAction("fetching Interviewers"));

    const resp = await api.getInterviewersList(getUserToken(getState()));
    if (resp.status === 0) {
      let data = {
        interviewers: []
      };
      resp.staff.forEach((singleStaff) => {
        data.interviewers.push({
          staff_id: singleStaff.staff_id,
          fullName: singleStaff.full_name
        })
      });

      dispatch({
        type: types.INTERVIEWERS_FETCHED,
        ...data
      });
      dispatch(actionCenter.finishAction("fetching Interviewers"));
    } else {
      dispatch(actionCenter.failAction("fetching Interviewers", resp.error_message));
    }
  }
}

export function createManagerProfile(fullName, email, password) {
  return async (dispatch, getState) => {
    dispatch(actionCenter.startAction('create manager profile'));

    const resp = await api.newManager(getUserToken(getState()), {
      fullName,
      email,
      passwordHash: password // ToDo hash password
    });

    if (resp.status === 0) {
      dispatch({
        type: types.MANAGER_CREATED,
        manager: {
          staff_id: resp.id,
          fullName
        }
      });
      dispatch(actionCenter.finishAction('create manager profile', 'Created successfully'));
      setTimeout(() => dispatch(actionCenter.popMessage()), 3000);
    } else {
      dispatch(actionCenter.failAction('create manager profile', resp.error_message));
    }
  }
}

export function createInterviewerProfile(fullName, email, password) {
  return async (dispatch, getState) => {
    dispatch(actionCenter.startAction('create interviewer profile'));

    const resp = await api.newInterviewer(getUserToken(getState()), {
      fullName,
      email,
      passwordHash: password // ToDo hash password
    });

    if (resp.status === 0) {
      dispatch({
        type: types.INTERVIEWER_CREATED,
        interviewer: {
          staff_id: resp.id,
          fullName
        }
      });
      dispatch(actionCenter.finishAction('create interviewer profile', 'Created successfully'));
      setTimeout(() => dispatch(actionCenter.popMessage()), 3000);
    } else {
      dispatch(actionCenter.failAction('create interviewer profile', resp.error_message));
    }
  }
}

export function deleteInterviewerProfile(staff_id) {
  return async (dispatch, getState) => {
    dispatch(actionCenter.startAction('deleting interviewer profile'));

    const resp = await api.deleteInterviewer(getUserToken(getState()), staff_id);

    if (resp.status === 0) {
      dispatch({
        type: types.INTERVIEWER_DELETED,
      });
      dispatch(fetchInterviewersFromServer(getUserToken(getState())));

      dispatch(actionCenter.finishAction('deleting interviewer profile', 'Deleted successfully'));
      setTimeout(() => dispatch(actionCenter.popMessage()), 3000);
    } else {
      dispatch(actionCenter.failAction('deleting interviewer profile', resp.error_message));
    }
  }
}

export function deleteManagerProfile(staff_id) {
  return async (dispatch, getState) => {
    dispatch(actionCenter.startAction('deleting interviewer profile'));

    const resp = await api.deleteManager(getUserToken(getState()), staff_id);

    if (resp.status === 0) {
      dispatch({
        type: types.INTERVIEWER_DELETED,
      });
      dispatch(fetchManagersFromServer(getUserToken(getState())));

      dispatch(actionCenter.finishAction('deleting interviewer profile', 'Deleted successfully'));
      setTimeout(() => dispatch(actionCenter.popMessage()), 3000);
    } else {
      dispatch(actionCenter.failAction('deleting interviewer profile', resp.error_message));
    }
  }
}

