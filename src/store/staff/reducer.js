import * as types from './actionTypes';

// --------------------------------------------------
//
//  Initial state of this part
//
// --------------------------------------------------

const initialState = {
  managers: [],
  interviewers: []
};

// --------------------------------------------------
//
//  Reducer for this part
//
// --------------------------------------------------

export default function reduce(state = initialState, action) {
  switch (action.type) {
    case types.MANAGERS_FETCHED:
      return {
        ...state,
        managers: action.managers
      };
    case types.INTERVIEWERS_FETCHED:
      return {
        ...state,
        interviewers: action.interviewers
      };

    case types.MANAGER_CREATED:
      return {
        ...state,
        managers: [action.manager, ...state.managers]
      };
    case types.INTERVIEWER_CREATED:
      return {
        ...state,
        interviewers: [action.interviewer, ...state.interviewers]
      };
    default:
      return state;
  }
}

// --------------------------------------------------
//
//  Selectors for this part
//
// --------------------------------------------------

export function getManagers(state) {
  return state.staff.managers;
}

export function getInterviewers(state) {
  return state.staff.interviewers;
}
