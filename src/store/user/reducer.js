import * as types from './actionTypes';

// --------------------------------------------------
//
//  Initial state of this part
//
// --------------------------------------------------

const initialState = {
  token: undefined,
  id: -1,
  fullName: '',
  accountType: -1
};

// --------------------------------------------------
//
//  Reducer for this part
//
// --------------------------------------------------

export default function reduce(state = initialState, action) {
  switch (action.type) {
    case types.TOKEN_FETCHED:
      return {
        ...state,
        token: action.token,
        id: action.id,
        fullName: action.fullName,
        accountType: action.accountType
      };
    case types.MANAGERS_FETCHED:
      return {
        ...state,
        managers: action.managers
      };
    case types.INTERVIEWERS_FETCHED:
      return {
        ...state,
        interviewers: action.interviewers
      };
    case types.TOKEN_INVALIDATED:
      return {
        ...state,
        token: undefined,
        id: -1
      };
    default:
      return state;
  }
}

// --------------------------------------------------
//
//  Selectors for this part
//
// --------------------------------------------------

export function getUserToken(state) {
  return state.user.token;
}

export function getUserType(state) {
  return state.user.accountType;
}

export function getUserFullName(state) {
  return state.user.fullName;
}

export function getManagers(state) {
  return state.user.managers;
}

export function getInterviewers(state) {
  return state.user.interviewers;
}

export function getUserID(state) {
  return state.user.id;
}
