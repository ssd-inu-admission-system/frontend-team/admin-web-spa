import * as Cookies from 'js-cookie';

import * as api from '../../services/apiConnector';
import * as types from './actionTypes';

import * as actionCenter from '../actionCenter/actions';

export function fetchTokenFromCookies() {
    return (dispatch, getState) => {
        let data = Cookies.get('user-data');
        if (typeof (data) == 'string') {
            data = JSON.parse(data);
            dispatch({
                type: types.TOKEN_FETCHED,
                ...data
            });
        }
    }
}

export function fetchTokenFromServer(email, password) {
    return async (dispatch, getState) => {
        // Start fetching process
        dispatch(actionCenter.startAction("fetching token"));

        // Send info to the server
        const resp = await api.signIn(email, password);
        if (resp.status === 0) {
            const data = {
                token: resp.token,
                fullName: resp.full_name,
                accountType: resp.account_type,
                id: resp.id,
            };

            // Place data in cookies
            Cookies.set('user-data', JSON.stringify(data));

            // Resolve fetching process
            dispatch({
                type: types.TOKEN_FETCHED,
                ...data
            });
            dispatch(actionCenter.finishAction("fetching token"));
        } else {
            dispatch(actionCenter.failAction("fetching token", resp.error_message));
        }
    }
}

export function invalidateToken() {
    return (dispatch, getState) => {
        Cookies.remove('user-data');
        dispatch({type: types.TOKEN_INVALIDATED});
    }
}
