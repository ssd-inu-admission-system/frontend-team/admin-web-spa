// Here we export our store


import staff from './staff/reducer';
import user from './user/reducer';
import actionCenter from './actionCenter/reducer';
import tests from './tests/reducer';
import applications from './applications/reducer';

export {
    staff,
    user,
    actionCenter,
    applications,
    tests,
};