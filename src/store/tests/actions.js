import * as api from '../../services/apiConnector';
import * as types from './actionTypes';

import * as actionCenter from '../actionCenter/actions';

import {getUserToken} from "../user/reducer";


export function createNewTest(test) {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction("creating new test"));

        const resp = await api.createNewTest(test, getUserToken(getState()));
        if (resp.status === 0) {
            dispatch(actionCenter.finishAction("creating new test"));
        } else {
            dispatch(actionCenter.failAction("creating new test", resp.error_message));
        }
    }
}

export function fetchTestsFromServer() {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction("fetching tests"));

        const resp = await api.getTests(getUserToken(getState()));
        if (resp.status === 0) {

            dispatch({
                type: types.TESTS_FETCHED,
                tests: resp.tests
            });
            dispatch(actionCenter.finishAction("fetching tests"));
        } else {
            dispatch(actionCenter.failAction("fetching tests", resp.error_message));
        }
    }
}

export function deleteTest(id) {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction("deleting test"));

        const resp = await api.deleteTest(getUserToken(getState()), id);
        if (resp.status === 0) {
            dispatch({
                type: types.TEST_INFO_DELETE,
                tests: resp.tests
            });
            dispatch(fetchTestsFromServer());
            dispatch(actionCenter.finishAction("deleting test"));
        } else {
            dispatch(actionCenter.failAction("deleting test", resp.error_message));
        }
    }
}

export function fetchTestInfoFromServer(id) {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction("fetching test info"));

        const resp = await api.getTestInfo(getUserToken(getState()), id);
        if (resp.status === 0) {
            dispatch({
                type: types.TEST_INFO_FETCHED,
                testInfo: {
                    test_id: id,
                    title: resp.title,
                    description: resp.description,
                    program_id: resp.program_id,
                    questions: resp.questions
                }
            });
            dispatch(actionCenter.finishAction("fetching test info"));
        } else {
            dispatch(actionCenter.failAction("fetching test info", resp.error_message));
        }
    }
}

export function updateTest(id, test) {
    return async (dispatch, getState) => {
        dispatch(deleteTest(id));
        dispatch(createNewTest(test));
        /*
        dispatch(actionCenter.startAction("updating test info"));

        const resp = await api.updateTest(getUserToken(getState()), id, test);
        if (resp.status === 0) {
            dispatch({
                type: types.TEST_INFO_UPDATE
            });
            dispatch(actionCenter.finishAction("updating test info"));
        } else {
            dispatch(actionCenter.failAction("updating test info", resp.error_message));
        }
        */
    }
}

export function removeCurrentInfoTest() {
    return async (dispatch, getState) => {
            dispatch({
                type: types.TEST_INFO_REMOVE
            });
    }
}
