export const TESTS_FETCHED = 'test.TESTS_FETCHED';
export const TEST_INFO_FETCHED = 'test.TEST_INFO_FETCHED';
export const TEST_INFO_REMOVE = 'test.TEST_INFO_REMOVE'; // пока вообще не используется, и может не будет
export const TEST_INFO_UPDATE = 'test.TEST_INFO_UPDATE'; // ничего не случается
export const TEST_INFO_DELETE = 'test.TEST_INFO_DELETE'; // ничего не случается