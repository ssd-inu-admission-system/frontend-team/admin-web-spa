import * as types from './actionTypes';

// --------------------------------------------------
//
//  Initial state of this part
//
// --------------------------------------------------

const initialState = {
    testsList: undefined, // undefined means - tests wasn't loaded from server
    testInfo: undefined, // undefined means - test wasn't loaded from server
};

// --------------------------------------------------
//
//  Reducer for this part
//
// --------------------------------------------------

export default function reduce(state = initialState, action) {
    switch (action.type) {
        case types.TESTS_FETCHED:
            return {
                ...state,
                testsList: action.tests
            };
        case types.TEST_INFO_FETCHED:
            return {
                ...state,
                testInfo: action.testInfo
            };
        default:
            return state;
    }
}

// --------------------------------------------------
//
//  Selectors for this part
//
// --------------------------------------------------

export function getTests(state) {
    return state.tests.testsList;
}

export function getTestsInfo(state) {
    return state.tests.testInfo;
}
