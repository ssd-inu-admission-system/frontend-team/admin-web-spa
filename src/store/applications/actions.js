import * as api from '../../services/apiConnector';
import * as types from './actionTypes';

import * as actionCenter from '../actionCenter/actions';

import {getUserID, getUserToken} from "../user/reducer";

export function fetchApplications() {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction('fetching applications'));

        const resp = await api.getApplications(getUserToken(getState()));

        if (resp.status === 0) {
            dispatch({
                type: types.APPLICATIONS_FETCHED,
                applications: resp.applications
            });
            dispatch(actionCenter.finishAction('fetching applications'));
        } else {
            dispatch(actionCenter.failAction('fetching applications', resp.error_message));
        }
    }
}

export function removeCurrentApplication() {
    return {
        type: types.REMOVE_CURRENT_APPLICATION,
    };
}

export function fetchApplication(applicationId) {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction(`fetching application ${applicationId}`));

        const applicationInfo = await api.getApplication(applicationId, getUserToken(getState()));

        if (applicationInfo.status === 0) {

            const userInfo = await api.getUserInfo(getUserToken(getState()), applicationInfo.applicant_id);

            if (userInfo.status === 0) {
                dispatch({
                    type: types.SINGLE_APPLICATION_FETCHED,
                    application: applicationInfo,
                    user: userInfo
                });
            } else {
                dispatch(actionCenter.failAction(`fetching application ${applicationId}`, userInfo.error_message));
            }
            dispatch(actionCenter.finishAction(`fetching application ${applicationId}`));
        } else {
            dispatch(actionCenter.failAction(`fetching application ${applicationId}`, applicationInfo.error_message));
        }
    }
}

export function changeApplicationStatus(applicationId, statusCode) {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction(`changing application status id: ${applicationId}, status: ${statusCode}`));

        const resp = await api.changeApplicationStatus(applicationId, Number(statusCode), getUserToken(getState()));

        if (resp.status === 0) {
            dispatch(actionCenter.finishAction(`changing application status id: ${applicationId}, status: ${statusCode}`));
            dispatch(fetchApplication(applicationId));
        } else {
            dispatch(actionCenter.failAction(`changing application status id: ${applicationId}, status: ${statusCode}`, resp.error_message));
        }
    }
}

export function submitGradeToApplication(applicationID, grade) {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction('submitGradeToApplication'));

        const resp = await api.sendApplicationGrade(applicationID, getUserToken(getState()), grade);

        if (resp.status === 0) {
            dispatch({
                type: types.APPLICATION_GRADE_SENT,
            });
            fetchApplication(applicationID);
            dispatch(actionCenter.finishAction('submitGradeToApplication', 'Grade has been successfully updated'));
            setTimeout(() => dispatch(actionCenter.popMessage()), 3000);
        } else {
            dispatch(actionCenter.failAction('submitGradeToApplication', resp.error_message));
        }
    }
}

export function submitCommentToApplication(applicationID, comment) {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction('submitCommentToApplication'));

        const resp = await api.sendApplicationComment(applicationID, getUserToken(getState()), comment);

        if (resp.status === 0) {
            dispatch({
                type: types.APPLICATION_COMMENT_SENT,
            });
            fetchApplication(applicationID);
            dispatch(actionCenter.finishAction('submitCommentToApplication', 'Comment has been successfully updated'));
            setTimeout(() => dispatch(actionCenter.popMessage()), 3000);
        } else {
            dispatch(actionCenter.failAction('submitCommentToApplication', resp.error_message));
        }
    }
}

export function assignInterviewer(interviewerID, applicationID) {
    return async (dispatch, getState) => {
        dispatch(actionCenter.startAction('assignInterviewer'));

        const resp = await api.assignInterviewer(applicationID, getUserToken(getState()), interviewerID);

        if (resp.status === 0) {
            dispatch({
                type: types.APPLICATION_COMMENT_SENT,
            });
            fetchApplication(applicationID);
            dispatch(actionCenter.finishAction('assignInterviewer'));
        } else {
            dispatch(actionCenter.failAction('assignInterviewer', resp.error_message));
        }
    }
}
