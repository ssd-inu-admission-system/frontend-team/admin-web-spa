import * as types from './actionTypes';

// --------------------------------------------------
//
//  Initial state of this part
//
// --------------------------------------------------

const initialState = {
    applications: [],
    currentApplication: {}
};

// --------------------------------------------------
//
//  Reducer for this part
//
// --------------------------------------------------

export default function reduce(state = initialState, action) {
    switch (action.type) {
        case types.APPLICATIONS_FETCHED:
            return {
                ...state,
                applications: action.applications
            };
        case types.SINGLE_APPLICATION_FETCHED:
            return {
                ...state,
                currentApplication: {
                    application: action.application,
                    user: action.user
                }
            };
        case types.APPLICATION_COMMENT_SENT:
            return state;
        case types.APPLICATION_GRADE_SENT:
            return state;
        case types.REMOVE_CURRENT_APPLICATION:
            return {
                ...state,
                currentApplication: {}
            };
        default:
            return state;
    }
}

// --------------------------------------------------
//
//  Selectors for this part
//
// --------------------------------------------------

export function getApplications(state) {
    return state.applications.applications;
}

export function getCurrentApplication(state) {
    return state.applications.currentApplication;
}
