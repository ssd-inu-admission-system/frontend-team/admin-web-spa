import * as types from './actionTypes';

// --------------------------------------------------
//
//  Initial state of this part
//
// --------------------------------------------------

const initialState = {
  errors: [],
  messages: [],
  actions: []
};

// --------------------------------------------------
//
//  Reducer for this part
//
// --------------------------------------------------

export default function reduce(state = initialState, action) {
  switch (action.type) {
    case types.ACTION_STARTED:
      return {
        ...state,
        actions: [...state.actions, action.actionName]
      };
    case types.ACTION_FAILED:
      const newActions = [...state.actions];
      const ind = newActions.indexOf(action.actionName);
      newActions.splice(ind, 1);
      return {
        ...state,
        actions: newActions,
        errors: action.error ? [...state.errors, action.error] : state.errors
      };
    case types.ACTION_FINISHED:
      const newActionsF = [...state.actions];
      const indF = newActionsF.indexOf(action.actionName);
      newActionsF.splice(indF, 1);
      return {
        ...state,
        actions: newActionsF,
        messages: action.message ? [...state.messages, action.message] : state.messages
      };
    case types.ERROR_PUSHED:
      return {
        ...state,
        errors: [...state.errors, action.error]
      };
    case types.ERROR_POPPED:
      const newErrors = [...state.errors];
      newErrors.shift();
      return {
        ...state,
        errors: newErrors
      };
    case types.MESSAGE_PUSHED:
      return {
        ...state,
        messages: [...state.message, action.message]
      };
    case types.MESSAGE_POPPED:
      const newMessages = [...state.messages];
      newMessages.shift();
      return {
        ...state,
        messages: newMessages
      };
    default:
      return state;
  }
}

// --------------------------------------------------
//
//  Selectors for this part
//
// --------------------------------------------------

export function getFirstError(state) {
  return state.actionCenter.errors[0];
}

export function getFirstMessage(state) {
  return state.actionCenter.messages[0];
}

export function isActionInProgress(state) {
  return !!state.actionCenter.actions.length;
}
