import * as types from './actionTypes';

export function startAction(actionName) {
  return {
    type: types.ACTION_STARTED,
    actionName
  }
}

export function finishAction(actionName, message) {
  return {
    type: types.ACTION_FINISHED,
    actionName,
    message
  }
}

export function failAction(actionName, error) {
  return {
    type: types.ACTION_FAILED,
    actionName,
    error
  }
}

export function pushMessage(message) {
  return {
    type: types.MESSAGE_PUSHED,
    message
  }
}

export function popMessage() {
  return {
    type: types.MESSAGE_POPPED
  }
}

export function pushError(error) {
  return {
    type: types.ERROR_PUSHED,
    error
  }
}

export function popError() {
  return {
    type: types.ERROR_POPPED
  }
}
