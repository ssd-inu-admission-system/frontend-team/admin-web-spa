import React from "react";

import styles from "./testTitleForm.module.css";

export const TestTitleForm = (props) => {
    return (
        <div>
            <div className={styles.headerInput}>Test Name</div>
            <input
                type="text"
                className={styles.headerInput}
                placeholder="Test Name"
                onChange={props.onTitleChange}
                value={props.title}/>
            <div className={styles.headerInput}>Description</div>
            <input
                type="text"
                className={styles.headerInput}
                placeholder="Description"
                onChange={props.onDescriptionChange}
                value={props.description}/>
            {/*
                <div className={styles.questionBox}>
                    <input type="text" className={styles.questionName} placeholder="Question header"/>
                    <select name="typeOfChoice" className={styles.choices}>
                        <option value="single">Single Choice</option>
                        <option value="multiple">Multiple Choice</option>
                    </select>
                    <input type="radio" className={styles.questionOption} id="option"/>
                    <label htmlFor="option">
                        <input type="text" className={styles.optionName} placeholder="Option" />
                    </label>
                </div>
                <div>
                    <span className={styles.buttonName}>SAVE TEST</span>
                </div>
                    */}
        </div>
    );
};