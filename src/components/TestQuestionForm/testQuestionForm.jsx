import React from "react";

import delIcon from '../../assets/icons/delIcon.png';

import styles from "./testQuestionForm.module.css";

/**
 question - object what describes question
 {
    text: "",
    type: 1/2, //1- single; 2 - multiple choice
    options: [
        {
            "text": string,
            "is_correct": bool
        }
    ]
 }
 onQuestionChange("text" or "type", event.target.newValue)
 onQuestionOptionChange(optionID, "text" or "is_correct", event.target.newValue)
 */


export class TestQuestionForm extends React.Component {

    onChangeCreator = (key) => (event) => {
        switch (key) {
            case "text":
                this.props.onQuestionChange("text", event.target.value);
                break;
            case "type":
                this.props.onQuestionChange("type", Number(event.target.value));
                break;
        }
    };

    onOptionChangeCreator = (index, key) => (event) => {
        let newOptions = [...this.props.question.options];
        switch (key) {
            case "text":
                newOptions[index][key] = event.target.value;
                break;
            case "is_correct": // can't select last element
                if (this.props.question.type === 1) { // set all to false if single choice question
                    newOptions = this.props.question.options.map((object) => {
                        return {text: object.text, is_correct: false};
                    });
                }
                newOptions[index][key] = !newOptions[index][key];
                break;
            case "remove":
                newOptions.splice(index, 1);
                break;
            case "add":
                newOptions.push({text: "", is_correct: false});
                break;
        }
        this.props.onQuestionChange("options", newOptions);
    };

    render() {
        if (this.props.question.type === 0) {
            return (
                <div className={styles.questionBox}>
                    Unsupported question type
                </div>);
        }
        const type = this.props.question.type === 1 ? 'radio' : 'checkbox';
        return (
            <div className={styles.container}>
                <div className={styles.questionBox}>
                    <input type="text" className={styles.questionName}
                           placeholder="Question header"
                           onChange={this.onChangeCreator("text")}
                           value={this.props.question.text}/>
                    <select name="typeOfChoice"
                            onChange={this.onChangeCreator("type")}
                            value={this.props.question.type}
                            className={styles.choices}>
                        <option value={1}>Single Choice</option>
                        <option value={2}>Multiple Choice</option>
                    </select>
                    <form>
                        {this.props.question.options.map((option, index) => {
                            return (
                                <div key={index}>
                                    <input type={type} className={styles.questionOption}
                                           checked={this.props.question.options[index].is_correct}
                                           onChange={this.onOptionChangeCreator(index, "is_correct")}/>
                                    <input type="text" className={styles.questionName}
                                           value={this.props.question.options[index].text}
                                           onChange={this.onOptionChangeCreator(index, "text")}/>
                                    <img className={styles.iconButton}
                                         src={delIcon} alt={"delete"}
                                         onClick={this.onOptionChangeCreator(index, "remove")}/>
                                </div>
                            );
                        })}
                    </form>
                    <input type={type} className={styles.questionOption} disable="true"/>
                    <div onClick={this.onOptionChangeCreator(0, "add")} className={styles.addOption}>
                        <span className={styles.newOption}>New Option...</span>
                    </div>
                </div>
                <div className={styles.icons}>
                    <img className={styles.iconButton}
                         src={delIcon} alt={"delete"}
                         onClick={this.props.onQuestionDelete}/>
                    {/* <img className={styles.iconButton}
                         src={add} alt={"add"}
                         onClick={this.onOptionChangeCreator(0, "add")}/> */}
                </div>
            </div>
        );
    }
}
