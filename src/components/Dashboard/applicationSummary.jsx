import React from 'react';
import styles from './applicationSummary.module.css'
import person from './../../assets/round1.png'

const ApplicationSummary = () => {
    return (
        <div>
        <div className={styles.container}>
            <img className={styles.userPhoto} src={person} alt="person" />
            <div className={styles.personInfo}>
                <span className={styles.nameSurname}> Name Surname</span>
                <span className={styles.id}> ID 984537 </span>
            </div>
            <div className={styles.status}>
                Status: 
                <span className={styles.statusValue}>Applied</span>
            </div>
        </div>

        <div className={styles.container}>
            <img className={styles.userPhoto} src={person} alt="person" />
            <div className={styles.personInfo}>
                <span className={styles.nameSurname}> Name Surname</span>
                <span className={styles.id}> ID 984537 </span>
            </div>
            <div className={styles.status}>
                Status: 
                <span className={styles.statusValue}>Applied</span>
            </div>
        </div>
        </div>
    )
}

export default ApplicationSummary;