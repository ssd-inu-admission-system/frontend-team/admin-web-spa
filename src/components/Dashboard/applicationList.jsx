import React from 'react';
import ApplicationSummary from './applicationSummary';
import { Link } from 'react-router-dom';

export class ApplicationList extends React.Component {
    render() {
        return (
                <Link to={'/application/'} >
                    <ApplicationSummary />
                </Link>
        )
    }
}

export default ApplicationList;