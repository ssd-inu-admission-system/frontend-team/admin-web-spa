import React from 'react';
import { ApplicationWrapper } from '../ApplicationWrapper/applicationWrapper';
import styles from './applicationDetails.module.css';
import person from './../../assets/round1.png'

export class ApplicationDetails extends React.Component {
    render() {
        return (
            <ApplicationWrapper>
                <div className={styles.appliccantInfo}>
                    <div className={styles.container}>
                        <img className={styles.userPhoto} src={person} alt="person" />
                        <div className={styles.personInfo}>
                            <span className={styles.nameSurname}> Name Surname,</span>
                            <span className={styles.actualStatus}> applied </span>
                        </div>
                        <div className={styles.changeStatus}>
                            <span>Change Status</span>
                        </div>
                    </div>
                </div>
                <div className={styles.applicantDetails}>
                    <div className={styles.boxContainer}>
                        <span>Personal Data</span>
                    </div>
                    <div className={styles.boxContainer}>
                        <span>Education</span>
                    </div>
                    <div className={styles.boxContainer}>
                        <span>Documents</span>
                    </div>
                    <div className={styles.boxContainer}>
                        <span>Tests</span>
                    </div>
                </div>
            </ApplicationWrapper>

        )
    }
}

export default ApplicationDetails;