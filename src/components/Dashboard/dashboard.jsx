import React from 'react';
import ApplicationList from './applicationList';
import { ApplicationWrapper } from '../ApplicationWrapper/applicationWrapper';

export class Dashboard extends React.Component {
    render() {
        return (
            <ApplicationWrapper>
                <ApplicationList />
            </ApplicationWrapper>
        )
    }
}
