import React from 'react';

import classes from './Snackbar.module.css';

const Snackbar = props => {
  let additionalClass = '';
  if (props.type === 'error') {
    additionalClass = classes.red;
  } else {
    additionalClass = classes.green;
  }

  return (
    <div className={classes.snackBarContainer}>
      <div className={classes.snackBar + ' ' + additionalClass}>
        <span className={classes.text}>{props.children}</span>
        <button onClick={props.onClickHandler} className={classes.closeButton}>X</button>
      </div>
    </div>
  )
};

export default Snackbar;
