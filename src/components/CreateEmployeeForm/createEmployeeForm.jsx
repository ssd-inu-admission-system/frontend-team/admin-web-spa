import React from "react";

import styles from "./createEmployeeForm.module.css";

export const CreateEmployeeForm = (props) => {
    return (
        <form className={styles.formContainer} onSubmit={props.onSignUpSubmit}>
            {/* <p className={styles.personalDataText}>Create new Employee</p> */}
            <TextField title="Full Name"
                       label="f_name"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <TextField title="Email"
                       label="email"
                       type="email"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <TextField title="Password"
                       label="pass"
                       type="password"
                       data={props.data}
                       handleChange={props.handleChange}/>
            <input className={styles.nextSectionButton}
                   type={"submit"}
                   value={"Create"}/>
        </form>
    );
};


const TextField = (props) => {
    return (
        <div>
            <p className={styles.textFieldTitle}>{props.title}</p>
            <input className={styles.textFieldInput}
                   type={props.type || "text"}
                   placeholder={props.title}
                   value={props.data[props.label] || ""}
                   onChange={props.handleChange(props.label)}/>
        </div>
    );
};
