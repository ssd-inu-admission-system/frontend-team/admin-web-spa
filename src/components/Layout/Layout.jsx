import React from 'react';
import styles from './Layout.module.css';

import TopBar from './TopBar/TopBar';
import SideBar from './SideBar/SideBar';
import {getUserFullName, getUserToken, getUserType} from "../../store/user/reducer";
import {connect} from "react-redux";
import {invalidateToken} from "../../store/user/actions";

const Layout = (props) => {
    const menuItems = [
        {
            title: "Applications",
            link: "/application"
        },
    ];

    if (props.accountType === 0) { // 0 - is super admin
        menuItems.push({
            title: "Manage Staff",
            link: "/staff"
        });
        menuItems.push({
            title: "Interviewers List",
            link: "/interviewers"
        });
        menuItems.push({
            title: "Manage Tests",
            link: "/test"
        });
    }

    if (props.accountType === 1) { // 1 - is manager
        menuItems.push({
            title: "Manage Tests",
            link: "/test"
        });
    }

    return (
        <div className={styles.container}>
            <SideBar
                menuItems={menuItems}
                logoutHandler={() => props.dispatch(invalidateToken())}
                logoutLink="/"
                settingsLink="/"
                personName={props.fullName}
                personStatus={props.accountType}
            />
            <div className={styles.mainPart}>
                <TopBar/>
                <div className={styles.content}>
                    {props.children}
                </div>
            </div>
        </div>
    )
};


const mapStateToProps = (state) => {
    return {
        token: getUserToken(state),
        fullName: getUserFullName(state),
        accountType: getUserType(state)
    }
};

export default connect(mapStateToProps)(Layout);
