import React from 'react';

import classes from './TopBar.module.css';

const topBar = props => {
  return (
    <div className={classes.topBar}>
      {props.children}
    </div>
  );
};

export default topBar;
