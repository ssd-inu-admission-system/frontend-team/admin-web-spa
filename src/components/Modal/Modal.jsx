import React from 'react';

import classes from './Modal.module.css';

const modal = props => {
    return (
        <div className={classes.container}>
            <div className={classes.shadow} onClick={props.onCancelClick}/>
            <div className={classes.modal}>
                {props.children}
                {props.onSaveClick ? (
                    <div className={classes.buttons}>
                        <button className={classes.saveButton} onClick={props.onSaveClick}>Save</button>
                        <button className={classes.cancelButton} onClick={props.onCancelClick}>Cancel</button>
                    </div>
                ) : ""}
            </div>
        </div>
    )
};

export default modal;
