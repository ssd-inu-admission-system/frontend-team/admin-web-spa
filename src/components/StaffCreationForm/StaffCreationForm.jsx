import React from 'react';

import classes from './StaffCreationForm.module.css';

const staffCreationForm = props => {
  return (
    <form className={classes.formContainer} onSubmit={props.onSignUpSubmit}>
      <TextField title="Full Name"
                 label="fullName"
                 data={props.data}
                 handleChange={props.handleChange}/>
      <TextField title="Email"
                 label="email"
                 type="email"
                 data={props.data}
                 handleChange={props.handleChange}/>
      <TextField title="Password"
                 label="password"
                 type="password"
                 data={props.data}
                 handleChange={props.handleChange}/>
    </form>
  );
};

const TextField = (props) => {
  return (
    <div>
      <p className={classes.textFieldTitle}>{props.title}</p>
      <input className={classes.textFieldInput}
             type={props.type || "text"}
             placeholder={props.title}
             value={props.data[props.label] || ""}
             onChange={props.handleChange(props.label)}/>
    </div>
  );
};

export default staffCreationForm;
