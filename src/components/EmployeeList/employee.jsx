import React from 'react';
import styles from './employee.module.css';
import person from './../../assets/round1.png'
import {LoadingWidget} from "../LoadingWIdget/loadingWidget";

export class EmployeeList extends React.Component {
    render() {
        if (this.props.workers) {
            return (
                <div>{
                    this.props.workers.map((worker) => {
                        return (
                            <div className={styles.container} key={worker.staff_id}>
                                <img className={styles.userPhoto} src={person} alt="person"/>
                                <div className={styles.personInfo}>
                                    <span className={styles.nameSurname}>{worker.fullName}</span>
                                </div>
                                <div className={styles.delete} >
                                    <span onClick={this.props.onWorkerDelete(worker.staff_id)}>Delete</span>
                                </div>
                            </div>
                        );
                    })}
                </div>
            )
        } else {
            return (<LoadingWidget/>);
        }
    }
}
