import React from "react";

import styles from "./signInForm.module.css";

/* List of keys that will be passed
email
password
 */
export const SignInForm = (props) => {
    return (
        <form className={styles.formContainer} onSubmit={props.onSubmit}>
            <p className={styles.signInText}>Sign In</p>
            <input className={styles.inputTextFiled}
                   type={"email"}
                   placeholder="Email"
                   value={props.data["email"] || ""}
                   onChange={props.handleChange("email")}/>
            <input className={styles.inputTextFiled}
                   type={"password"}
                   placeholder="Password"
                   value={props.data["password"] || ""}
                   onChange={props.handleChange("password")}/>
            <input className={styles.signUpButton}
                   type={"submit"}
                   value={"SIGN IN"}/>
        </form>
    );
};
