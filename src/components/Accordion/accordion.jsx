import React from 'react';

import style from './accordion.module.css';

const styles = {
    active: {
        display: 'inherit'
    },
    inactive: {
        display: 'none'
    }
};

export class AccordionMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false
        };
        this.toggleItem = this.toggleItem.bind(this);
    }

    toggleItem(e) {
        this.setState({
            active: !this.state.active
        });
    }

    render() {
        const stateStyle = this.state.active ? styles.active : styles.inactive;
        let childrenWithProps = React.Children.map(this.props.children, (child) => {
            return React.cloneElement(child, {stateStyle: stateStyle});
        });

        return (
            <div className={style.titleBox} onClick={this.toggleItem}>
                <h1>{this.props.name}
                    {childrenWithProps}
                </h1>
            </div>
        );
    }
}

export class Accordion extends React.Component {
    render() {
        let childrenWithProps = React.Children.map(this.props.children, (child) => {
            return React.cloneElement(child, {stateStyle: this.props.stateStyle});
        });

        return (
            <div className={style.accordionTitle}>{this.props.title}
                {childrenWithProps}
            </div>
        );
    }
}

export class Panel extends React.Component {
    render() {
        return (
            <div className={style.panel} style={this.props.stateStyle}>
                <span>{this.props.title}</span>
                <div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}