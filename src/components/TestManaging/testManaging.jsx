import React from "react";
import {NavLink} from 'react-router-dom'

import styles from "./testManaging.module.css";

export const TestManaging = (props) => {
    return (
        <div>
            <div>
                {props.testsList.map((test) => {
                    return (
                        <div className={styles.container} key={test.test_id}>
                            <div>
                                <h3>{test.title || `test title of ${test.test_id} test`}</h3>
                                <span>{test.description || `test description of ${test.test_id} test`}</span>
                                <NavLink to={`/edit_test/${test.test_id}`} className={styles.link}>
                                        <div className={styles.buttonTest}>
                                            Edit test
                                        </div>
                                </NavLink>
                            </div>
                                <div className={styles.delete} onClick={props.onTestDelete(test.test_id)}>
                                    <span>Delete</span>
                                </div>
                        </div>
                    );
                })}
            </div>
            <div className={styles.buttonPadding}>
            <NavLink to='/new_test' className={styles.link}>
                <span className={styles.buttonName}>CREATE TEST</span>
            </NavLink>
            </div>
        </div>
    );
};
