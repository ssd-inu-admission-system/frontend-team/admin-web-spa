import React from "react";

import logo from "../../assets/university_logo.png";
import styles from "./loginStaticPage.module.css";

export const LoginStaticPage = (props) => {
    return (
        <div className={styles.staticDataContainer}>
            <div className={styles.horizontal}>
                 <img className={styles.icon} src={logo} alt={"Logo"}/>
                <p className={styles.universityName}>
                    INU University
                </p>
             </div>
         </div>
    );
};
