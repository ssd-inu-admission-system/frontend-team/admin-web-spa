import React, {Component} from 'react';
import {Switch, Route, withRouter, Redirect} from 'react-router-dom';
import Layout from './components/Layout/Layout';
import LoginPage from './containers/LoginPage/loginPage';
import {fetchTokenFromCookies} from "./store/user/actions";

import {connect} from 'react-redux';
import {getUserToken} from "./store/user/reducer";

import ManageStaffPage from './containers/ManageStuffPage/manageStaffPage';
import ManageInterviewersPage from './containers/ManageInterviewersPage/manageInterviewersPage';
import {getFirstError, getFirstMessage, isActionInProgress} from "./store/actionCenter/reducer";
import {popError, popMessage} from "./store/actionCenter/actions";
import Snackbar from "./components/Snackbar/Snackbar";
import Spinner from "./components/Spinner/Spinner";
import TestManagePage from './containers/TestManagePage/testManagePage';
import TestCreationPage from './containers/TestCreationPage/testCreationPage';
import ApplicationsListsPage from './containers/ApplicationsListsPage/applicationsListsPage';
import ApplicationsDetails from './containers/ApplicationsDetails/applicationsDetails';
import TestEditingPage from './containers/TestEditingPage/testEditingPage';


class App extends Component {
    constructor(props) {
        super(props);
        props.dispatch(fetchTokenFromCookies());
    }

    render() {
        return (
            <React.Fragment>
                {this.props.actionInProgress ? (<Spinner/>) : ''}
                {this.props.token ? (
                    <Layout>
                        <Switch>
                            <Route path="/staff" component={ManageStaffPage}/>
                            <Route path="/interviewers" component={ManageInterviewersPage}/>
                            <Route path="/test" component={TestManagePage}/>
                            <Route path="/new_test" component={TestCreationPage}/>
                            <Route path="/application" component={ApplicationsListsPage} exact/>
                            <Route path="/application/:id" component={ApplicationsDetails}/>
                            <Route path="/edit_test/:id" component={TestEditingPage}/>
                            <Redirect from="*" to="/application"/>
                        </Switch>
                    </Layout>
                ) : (
                    <LoginPage/>
                )}
                {this.props.error ? (<Snackbar
                    type={"error"}
                    onClickHandler={() => this.props.dispatch(popError())}
                >
                    {this.props.error}
                </Snackbar>) : ''}
                {this.props.message && !this.props.error ? (<Snackbar
                    type={"message"}
                    onClickHandler={() => this.props.dispatch(popMessage())}
                >
                    {this.props.message}
                </Snackbar>) : ''}
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: getUserToken(state),
        actionInProgress: isActionInProgress(state),
        error: getFirstError(state),
        message: getFirstMessage(state)
    }
};

export default withRouter(connect(mapStateToProps)(App));
